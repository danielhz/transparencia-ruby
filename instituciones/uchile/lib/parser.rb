# coding: utf-8
require 'nokogiri'
require 'rdf'
require 'rdf/ntriples'
include RDF

class Parser
  def initialize()
    @iterator = 0
    @graph = Graph.new
    @roles = {}
    @organizations = {}
    @organization_names = {
      '1101' => 'Rectoría',
      '1102' => 'Prorrectoría',
      '1103' => 'Vicerrectoría de Asuntos Académicos',
      '1104' => 'Vicerrectoría de Asuntos Económicos y Gestión Institucional',
      '1105' => 'Secretaría General',
      '1106' => 'Vicerrectoría de Investigación y Desarrollo',
      '1107' => 'Vicerrectoría de Extensión',
      '1108' => 'Vicerrectoría de Asuntos Estudiantiles y Comunitarios',
      '1201' => 'Facultad de Arquitectura y Urbanismo',
      '1202' => 'Facultad de Artes',
      '1203' => 'Facultad de Ciencias',
      '1204' => 'Facultad de Ciencias Agronómicas',
      '1205' => 'Facultad de Economía y Negocios',
      '1206' => 'Facultad de Ciencias Físicas y Matemáticas',
      '1207' => 'Facultad de Ciencias Forestales y de la Conservación de la Naturaleza',
      '1208' => 'Facultad de Ciencias Químicas y Farmacéuticas',
      '1209' => 'Facultad de Ciencias Sociales',
      '1210' => 'Facultad de Ciencias Veterinarias y Pecuarias',
      '1211' => 'Facultad de Derecho',
      '1212' => 'Facultad de Filosofía y Humanidades',
      '1213' => 'Facultad de Medicina',
      '1214' => 'Facultad de Odontología',
      '1301' => 'Instituto de Nutrición y Tecnología de los Alimentos',
      '1302' => 'Instituto de Estudios Internacionales',
      '1304' => 'Instituto de Asuntos Públicos',
      '1305' => 'Instituto de la Comunicación e Imagen',
      '1401' => 'Programa Académico de Bachillerato',
      '1402' => 'Liceo Experimental Manuel de Salas',
      '1501' => 'Hospital Clínico José Joaquín Aguirre',
      '1601' => 'Centro de Extensión Artística y Cultural',
      '1701' => 'Departamento de Evaluación, Medición y Registro Educacional',
      '1702' => 'Campus Juan Gómez Millas',
      '1801' => 'Convenio de Desempeño',
      '1802' => 'C.D. Iniciativa Bicentenario Campus Juan Gómez Millas',
    }
  end
  
  def parse(file_name)
    # Estos los prefijos de los recursos que se agregarán
    person       = RDF::URI.new "http://datosabiertos.cl/d/transparencia/Person/"
    membership   = RDF::URI.new "http://datosabiertos.cl/d/transparencia/Membership/"
    role         = RDF::URI.new "http://datosabiertos.cl/d/transparencia/Role/"
    organization = RDF::URI.new "http://datosabiertos.cl/d/transparencia/Organization/"

    # Estos son los prefijos del vocabulario
    voc          = RDF::URI.new "http://datosabiertos.cl/v/"
    org          = RDF::URI.new "http://www.w3.org/ns/org#"
    time         = RDF::URI.new "http://www.w3.org/2006/time#"
    foaf         = RDF::URI.new "http://xmlns.com/foaf/0.1/"
    rdf          = RDF::URI.new "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    skos         = RDF::URI.new "http://www.w3.org/2004/02/skos/core#"
    
    doc = File.open(file_name) { |f| Nokogiri::HTML(f) }
    doc.css('table.tabla > tbody > tr').each do |tr|
      @iterator += 1
      row = tr.css('td').map { |td| td.text }
      
      person_id = person.join(@iterator)
      @graph << [person_id, rdf + 'type', foaf.join('Person')]
      @graph << [person_id, foaf.join('name'), RDF::Literal.new("#{row[4]} #{row[2]} #{row[3]}".strip)]
      @graph << [person_id, voc.join('apellidoPaterno'), RDF::Literal.new(row[2])]
      @graph << [person_id, voc.join('apellidoMaterno'), RDF::Literal.new(row[3])] unless row[3].strip == ''
      @graph << [person_id, voc.join('nombres'), RDF::Literal.new(row[4])]

      if @roles.include? row[7]
        role_id = @roles[row[7]]
      else
        role_id = role.join("#{@roles.size + 1}")
        @roles[row[7]] = role_id
        @graph << [role_id, rdf + 'type', org + 'Role']
        @graph << [role_id, skos + 'prefLabel', RDF::Literal.new(row[7])]
      end

      if @organizations.include? row[16]
        organization_id = @organizations[row[16]]
      else
        organization_id = organization.join(row[16])
        @organizations[row[16]] = organization_id
        @graph << [organization_id, rdf + 'type', org + 'OrganizationalUnit']
        @graph << [organization_id, foaf.join('name'), RDF::Literal.new(@organization_names[row[16]])]
      end
      
      membership_id = membership.join(@iterator)
      @graph << [membership_id, rdf + 'type', org + 'Membership']
      @graph << [membership_id, org + 'member', person_id]
      @graph << [membership_id, voc + 'tipoDeContrato', RDF::Literal.new(row[0])]
      @graph << [membership_id, voc + 'estamento', RDF::Literal.new(row[1])]
      @graph << [membership_id, voc + 'grado', RDF::Literal.new(row[5])]
      @graph << [membership_id, voc + 'calificacion', RDF::Literal.new(row[6])] unless row[6].strip == 'Sin Información'
      @graph << [membership_id, org + 'role', role_id]
      @graph << [membership_id, voc + 'region', RDF::Literal.new(row[8])]
      @graph << [membership_id, voc + 'asignacionesEspeciales', RDF::Literal.new(row[9].to_i)]
      @graph << [membership_id, voc + 'unidadMonetaria', RDF::Literal.new(row[10])]
      @graph << [membership_id, voc + 'remuneracionBrutaMensual', RDF::Literal.new(row[11].to_i)]
      @graph << [membership_id, voc + 'horasExtrasDiurnas', RDF::Literal.new(row[12].to_i)]
      @graph << [membership_id, voc + 'horasExtrasNocturnas', RDF::Literal.new(row[13].to_i)]
      @graph << [membership_id, org + 'organization', organization_id]
      #puts row[14]
      @graph << [membership_id, voc + 'fechaInicio', RDF::Literal::Date.new(Date.strptime(row[14], '%d-%m-%Y'))]
      @graph << [membership_id, voc + 'fechaTermino', RDF::Literal::Date.new(Date.strptime(row[15], '%d-%m-%Y'))]
    end
  end

  def dump(format)
    @graph.dump(format)
  end
end
