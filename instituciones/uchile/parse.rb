require './lib/parser.rb'

parser = Parser.new

Dir['data/html/*.html'].each do |file_name|
  STDERR.puts "Parsing #{file_name}"
  parser.parse(file_name)
end

puts parser.dump(:ntriples)
