Transparencia
=============

Este repositorio contiene herramientas para descargar datos desde de
transparencia desde los sitios web de instituciones públicas de Chile
y convertirlos en archivos RDF.

Estructura del repositorio
--------------------------

1. La carpeta ´schema/´ contiene el RDF Schema del vocabulario utilizado en
   la publicación de los datos y su documentación.

2. La carpeta ´insituciones/´ contiene una carpeta para cada institución
   pública, con las herramientas necesarias para descargar y transformar a
   formato RDF los datos publicados por la institución. Las carpetas son
   nombradas con el dominio del sitio web de la institución. Por ejemplo,
   la carpeta ´instituciones/uchile' corresponde a la Universidad de Chile.